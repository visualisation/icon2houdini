# state file generated using paraview version 5.11.0-RC2-318-g1b5b4c3a3b
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1752, 1272]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.2750464895916256, 0.9050083671020597, 0.07475758957973228]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.1677974465588423, 2.256901033813877, 0.6156676779662833]
renderView1.CameraFocalPoint = [0.2750464895916257, 0.9050083671020598, 0.07475758957973222]
renderView1.CameraViewUp = [-0.5804598335097775, 0.6024943947658861, -0.5477836123484254]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.44205821459328326

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1752, 1272)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'CDIReader'
zghalf_surfacenc = CDIReader(registrationName='zghalf_surface.nc', FileNames=['/tmp/zghalf_surface.nc'])
zghalf_surfacenc.Dimensions = '(clon, clat, height)'
zghalf_surfacenc.CellArrayStatus = ['zghalf']
zghalf_surfacenc.SetProjection = 'Spherical Projection'
zghalf_surfacenc.LayerThickness = 50
zghalf_surfacenc.MaskingValueVar = 'zghalf'

# create a new 'Cell Data to Point Data'
cellDatatoPointData1 = CellDatatoPointData(registrationName='CellDatatoPointData1', Input=zghalf_surfacenc)
cellDatatoPointData1.CellDataArraytoprocess = ['zghalf']

# create a new 'Calculator'
calculator1 = Calculator(registrationName='Calculator1', Input=cellDatatoPointData1)
calculator1.CoordinateResults = 1
calculator1.Function = 'norm(coords) * (1 + zghalf / 6371000 * 30)'

# create a new 'Extract Surface'
extractSurface1 = ExtractSurface(registrationName='ExtractSurface1', Input=calculator1)

# create a new 'Transform'
transform1 = Transform(registrationName='Transform1', Input=extractSurface1)
transform1.Transform = 'Transform'

# init the 'Transform' selected for 'Transform'
transform1.Transform.Rotate = [-90.0, 0.0, 0.0]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from transform1
transform1Display = Show(transform1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
transform1Display.Representation = 'Surface'
transform1Display.ColorArrayName = [None, '']
transform1Display.SelectTCoordArray = 'None'
transform1Display.SelectNormalArray = 'None'
transform1Display.SelectTangentArray = 'None'
transform1Display.OSPRayScaleArray = 'zghalf'
transform1Display.OSPRayScaleFunction = 'PiecewiseFunction'
transform1Display.SelectOrientationVectors = 'None'
transform1Display.ScaleFactor = 0.06229447252171161
transform1Display.SelectScaleArray = 'None'
transform1Display.GlyphType = 'Arrow'
transform1Display.GlyphTableIndexArray = 'None'
transform1Display.GaussianRadius = 0.0031147236260855803
transform1Display.SetScaleArray = ['POINTS', 'zghalf']
transform1Display.ScaleTransferFunction = 'PiecewiseFunction'
transform1Display.OpacityArray = ['POINTS', 'zghalf']
transform1Display.OpacityTransferFunction = 'PiecewiseFunction'
transform1Display.DataAxesGrid = 'GridAxesRepresentation'
transform1Display.PolarAxes = 'PolarAxesRepresentation'
transform1Display.SelectInputVectors = [None, '']
transform1Display.WriteLog = ''
transform1Display.BumpMapInputDataArray = ['POINTS', 'zghalf']
transform1Display.ExtrusionInputDataArray = ['POINTS', 'zghalf']
transform1Display.InputVectors = [None, '']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
transform1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 3240.21923828125, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
transform1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 3240.21923828125, 1.0, 0.5, 0.0]

# ----------------------------------------------------------------
# restore active source
SetActiveSource(transform1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')